//
//  NotificationService.swift
//  NewsNotificationService
//
//  Created by Deepak Kumar on 11/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UserNotifications
import UIKit
import SDWebImage

/**
 This class will use to change the content of push notification before it is presented to user.
 Like to change title or body of notification OR add an attachment (like image) to push content.
 */

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)

        if let bestAttemptContent = bestAttemptContent {
          if let author = bestAttemptContent.userInfo["podcast-guest"] as? String {
            bestAttemptContent.title = "New Podcast: \(author)"
          }

          // get image url from payload
          guard let imageURLString =
            bestAttemptContent.userInfo["podcast-image"] as? String else {
            contentHandler(bestAttemptContent)
            return
          }

          // download image from the above url
          getMediaAttachment(for: imageURLString) { [weak self] image in
            guard
              let self = self,
              let image = image,
              let fileURL = self.saveImageAttachment(
                image: image,
                forIdentifier: "attachment.png")
              else {
                contentHandler(bestAttemptContent)
                return
            }

            // Create attachment
            let imageAttachment = try? UNNotificationAttachment(
              identifier: "image",
              url: fileURL,
              options: nil)

            // Set downloaded image to attachment of notification.
            if let imageAttachment = imageAttachment {
              bestAttemptContent.attachments = [imageAttachment]
            }

            // Call the content handler to deliver the push notification.
            contentHandler(bestAttemptContent)
          }

        }
    }

    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

    private func saveImageAttachment(
      image: UIImage,
      forIdentifier identifier: String
    ) -> URL? {

      let tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory())

      let directoryPath = tempDirectory.appendingPathComponent(
        ProcessInfo.processInfo.globallyUniqueString,
        isDirectory: true)

      do {
        try FileManager.default.createDirectory(
          at: directoryPath,
          withIntermediateDirectories: true,
          attributes: nil)

        let fileURL = directoryPath.appendingPathComponent(identifier)

        guard let imageData = image.pngData() else {
          return nil
        }

        try imageData.write(to: fileURL)
          return fileURL
        } catch {
          return nil
      }
    }

    private func getMediaAttachment(
      for urlString: String,
      completion: @escaping (UIImage?) -> Void
    ) {
      guard let url = URL(string: urlString) else {
        completion(nil)
        return
      }

        SDWebImageManager.shared.loadImage(with: url, options: .highPriority, progress: { (_, _, _) in

        }, completed: { (image, _, _, _, finished, _) in
            if let image = image, finished {
                completion(image)
            }
        })
    }

}
