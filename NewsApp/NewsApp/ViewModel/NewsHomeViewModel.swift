//
//  NewsHomeViewModel.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

typealias CompletionClosure = (() -> Void)
typealias FailedClosure = ((_ error: String?) -> Void)
typealias LoadMoreClosure = (() -> Void)
typealias ShowLoaderClosure = ((_ show: Bool) -> Void)
let pageSize = 20   //default page size

protocol NewsHomeViewModelProtocol {
    var completionHandler: CompletionClosure? { get set }
    var failureHandler: FailedClosure? { get set }
    var loadMoreHandler: LoadMoreClosure? { get set }
    var showLoaderHandler: ShowLoaderClosure? { get set }

    var newsItems: [NewsModel] { get set }
    var articlesCount: Int { get }

    func getNewsArticles(for category: String, refresh: Bool)
    func fetchNextPage(for cateogry: String, after itemNum: Int)
    func getNewsArticleAt(index: Int) -> NewsCellViewModel?
    func getNewsDetailViewModelAt(index: Int) -> NewsDetailsViewModel?
}

class NewsHomeViewModel: NewsHomeViewModelProtocol {
    var loadMoreHandler: LoadMoreClosure?
    var completionHandler: CompletionClosure?
    var failureHandler: FailedClosure?
    var showLoaderHandler: ShowLoaderClosure?

    private var totalResults: Int = 0
    var newsItems: [NewsModel] = [] {
        didSet {
            refreshData()
        }
    }

    var articlesCount: Int {
        newsItems.count
    }

    let dataManager: DataManagerProtocol = DataManager()

    func getNewsArticles(for category: String, pageNum: Int = 1) {
        dataManager.getNewsListFor(category: category, pageNum: pageNum, pageSize: pageSize, completed: { [weak self] (response) in
            guard let self = self else {
                return
            }

            self.showLoaderHandler?(false)
            if let response = response as? [String: Any] {      //If data received from server
                self.totalResults = response[ResponseKeys.totalResults] as? Int ?? 0

                if let articles = response[ResponseKeys.articles] as? [[String: Any]] {
                    if pageNum == 1 {
                        self.newsItems = REJsonDecoder.getModelArrayFromJsonArr(array: articles, type: NewsModel.self)
                        self.dataManager.clearNewsList()
                        self.dataManager.saveNewsArticles(articles: self.newsItems)
                    } else {
                        let newList = REJsonDecoder.getModelArrayFromJsonArr(array: articles, type: NewsModel.self)
                        self.newsItems.append(contentsOf: newList)
                        self.dataManager.saveNewsArticles(articles: newList)
                    }
                }
            } else if let response = response as? [NewsModel] {     //Data get from core data
                self.newsItems = response
            }
        }, failed: { [weak self] (error) in
            self?.failureHandler?(error)
            self?.showLoaderHandler?(false)
        })
    }

    func getNewsArticles(for category: String, refresh: Bool) {
        if !refresh {
            showLoaderHandler?(true)
        }
        getNewsArticles(for: category)
    }

    func fetchNextPage(for cateogry: String, after itemNum: Int) {
        if itemNum == articlesCount - 1, (itemNum + 1) < totalResults {     //If items received is less than total count then will get next page.
            loadMoreHandler?()

            let page = articlesCount / pageSize + 1
            getNewsArticles(for: cateogry, pageNum: page)
        }
    }

    func refreshData() {
        completionHandler?()
    }

    func getNewsArticleAt(index: Int) -> NewsCellViewModel? {
        NewsCellViewModel(with: newsItems[index])
    }

    func getNewsDetailViewModelAt(index: Int) -> NewsDetailsViewModel? {
        NewsDetailsViewModel(with: newsItems[index])
    }
}

class NewsCellViewModel: NSObject {
    var news: NewsModel?

    init?(with newsModel: NewsModel?) {
        guard let newsModel = newsModel else {
            return nil
        }

        news = newsModel
    }

    var title: String {
        return news?.source?.name ?? ""
    }

    var imgUrl: String? {
        return news?.urlToImage
    }

    var newsDescription: String? {
        return news?.description
    }
}
