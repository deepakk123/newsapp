//
//  NewsDetailsViewModel.swift
//  NewsApp
//
//  Created by Deepak Kumar on 02/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

class NewsDetailsViewModel: NSObject {

    var news: NewsModel?

    init?(with newsModel: NewsModel?) {
        guard let newsModel = newsModel else {
            return nil
        }

        news = newsModel
    }

    var name: String {
        return news?.source?.name ?? ""
    }

    var author: String {
        return NSLocalizedString("Author: ", comment: "") + (news?.author ?? "")
    }

    var imgUrl: String? {
        return news?.urlToImage
    }

    var newsUrl: String? {
        return news?.url
    }

    var newsDescription: String? {
        var description = ""
        let separator = "\n\n"
        if let title = news?.title {
            description.append(title)
        }
        if let desc = news?.description {
            description.append(separator + desc)
        }
        if let content = news?.content {
            description.append(separator + content)
        }

        return description
    }

    var publishedAt: String {
        guard let date = news?.publishedAt else {
            return ""
        }

        return NEDateFormatter.shared().getDateStringFrom(dateStr: date, format: DateFormat.newsDateFormat)
    }

}
