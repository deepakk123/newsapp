//
//  Logging.swift
//  NewsApp
//
//  Created by Deepak Kumar on 07/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import os

struct Logging {

    static let log = OSLog.init(subsystem: "com.nagarro.newsapp", category: "main")

    static func info(_ message: StaticString, _ args: CVarArg...) {
        os_log(.info, log: log, message, args)
    }

    static func debug(_ message: StaticString, _ args: CVarArg...) {
        os_log(.debug, log: log, message, args)
    }

    static func notice(_ message: StaticString, _ args: CVarArg...) {
        os_log(.default, log: log, message, args)
    }

    static func error(_ message: StaticString, _ args: CVarArg...) {
        os_log(.error, log: log, message, args)
    }

    static func fault(_ message: StaticString, _ args: CVarArg...) {
        os_log(.fault, log: log, message, args)
    }
}
