//
//  NEDateFormatter.swift
//  NewsApp
//
//  Created by Deepak Kumar on 02/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

import UIKit

class NEDateFormatter: NSObject {

    private static var sharedDateformatter: NEDateFormatter = {
        let dateFormatter = NEDateFormatter()
        return dateFormatter
    }()

    private lazy var dateFormatter: DateFormatter = {
        let tempDateFormatter = DateFormatter()
        return tempDateFormatter
    }()

    private lazy var iso8601DateFormatter: ISO8601DateFormatter = {
        let tempDateFormatter = ISO8601DateFormatter()
        return tempDateFormatter
    }()

    // MARK: Accessors

    class func shared() -> NEDateFormatter {
        return sharedDateformatter
    }

    func getDateStringFrom(dateStr: String, format: String) -> String {
        let date = iso8601DateFormatter.date(from: dateStr)

        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date!)
    }

    func getDateStringFrom(date: Date, format: String) -> String {
        dateFormatter.dateFormat = format
        let strDate = dateFormatter.string(from: date)
        return strDate
    }

    func getDateStringFrom(timeInterval: TimeInterval, format: String) -> String {
        let date = Date(timeIntervalSince1970: timeInterval)
        dateFormatter.dateFormat = format
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
}
