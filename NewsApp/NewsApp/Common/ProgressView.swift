//
//  ProgressView.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class ProgressView {

    let activityData = ActivityData()
    let load = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), type: .circleStrokeSpin, color: .black, padding: 0)

    func startAnimating(message: String? = nil) {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(self.activityData)

            if let msg = message {
                NVActivityIndicatorPresenter.sharedInstance.setMessage(msg)
            }
        }
    }

    func stopAnimating() {
        DispatchQueue.main.async {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
        }
    }

}
