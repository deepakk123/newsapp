//
//  Constants.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

struct ApiUrl {
    static let apiKey = "5d758d62e44041069ada393a767729fc"
    static let baseUrl = "https://newsapi.org/v2/top-headlines?country=us&apiKey=" + apiKey
}

struct RequestKeys {
    static let category = "category"
    static let page = "page"
    static let pageSize = "pageSize"
}

struct ResponseKeys {
    static let status = "status"
    static let totalResults = "totalResults"
    static let articles = "articles"
    static let message = "message"
}

struct DateFormat {
    static let newsDateFormat = "yyyy-MM-dd hh:mm"
}

struct CoreDataEntities {
    static let newsSource = "ArticleSource"
    static let article = "Article"
}

struct Identifiers {
    static let viewAction = "viewAction"
    static let newsCategory = "newsCategory"
}
