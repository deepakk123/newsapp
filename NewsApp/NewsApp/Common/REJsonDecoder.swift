//
//  REJsonDecoder.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

class REJsonDecoder: NSObject {

    class func getModelFromString<T: Decodable>(str:String, type:T.Type) -> T? {
        let model = getModelFromData(data: Data(str.utf8), type: type)
        return model
    }

    class func getModelFromDictionary<T: Decodable>(dict:[String:Any], type:T.Type) -> T? {
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: [])
            let model = getModelFromData(data: data, type: type)
            return model
        } catch {

        }
        return nil
    }

    class func getModelArrayFromJsonArr<T: Decodable>(array:[Any], type:T.Type) -> [T] {
        do {
            let data = try JSONSerialization.data(withJSONObject: array, options: [])
            let list = getModelListFromData(data: data, type: type)
            return list
        } catch {

        }
        return []
    }

    class func getModelFromData<T: Decodable>(data:Data, type:T.Type) -> T? {
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(type.self, from: data)
            return model
        } catch {

        }
        return nil
    }

    class func getModelListFromData<T: Decodable>(data:Data, type:T.Type) -> [T] {
        do {
            let decoder = JSONDecoder()
            let list = try decoder.decode([T].self, from: data)
            return list
        } catch {

        }
        return []
    }

    class func getJsonFromData(data: Data?) -> [String: Any]? {
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
            let jsonDictionary = jsonResult as? [String: Any]
            return jsonDictionary
        } catch {

        }
        return nil
    }
}
