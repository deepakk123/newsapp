//
//  DataManager.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import UIKit

typealias ResultBlock = (_ response: Any?) -> Void
typealias ErrorBlock = (_ error: String?) -> Void

protocol DataManagerProtocol {
    var completionHandler: ResultBlock! { get set }
    var failHandler: ErrorBlock! { get set }

    func getNewsListFor(category: String, pageNum: Int, pageSize: Int, completed:@escaping ResultBlock, failed:@escaping ErrorBlock)
    func clearNewsList()
    func saveNewsArticles(articles: [NewsModel])
}

class DataManager: DataManagerProtocol {
    var completionHandler: ResultBlock!
    var failHandler: ErrorBlock!

    var reachabilityManager: ReachabilityProtocol = ReachabilityManager.sharedInstance
    var apiManager: APIManagerProtocol = ApiManager()
    var dbManager: DBManagerProtocol = DBManager.sharedInstance

    func noInternetAlert() {
        UIApplication.topController?.showAlert(message: NSLocalizedString("You are not connected to internet. Please try again.", comment: ""))
    }

    func showErrorMsg(msg: String) {
        UIApplication.topController?.showAlert(message: msg)
    }

    func makeRequest(params: [String: Any]) {
        if reachabilityManager.isNetworkReachable() {        //If network is available
            apiManager.requestHTTPGET(url: ApiUrl.baseUrl, parameters: params, finished: {[weak self] (response) in

                if let data = response as? Data, let weakSelf = self {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                            // try to read out a articles array
                            if let success = json[ResponseKeys.status] as? String, success == "ok" {
                                Logging.debug("api result %@", json)
                                weakSelf.completionHandler(json)
                            } else {
                                if let msg = json[ResponseKeys.message] as? String {
                                    Logging.debug("api error message %@", msg)
                                    weakSelf.showErrorMsg(msg: msg)
                                    weakSelf.failHandler(msg)
                                }
                            }
                        }
                    } catch {
                        weakSelf.completionHandler([])
                    }
                }

            }, failed: {[weak self] (error) in
                self?.failHandler(error)
                Logging.error("error in getting api result %@", error?.description ?? "")
            })
        } else {
            noInternetAlert()
            failHandler(nil)
        }
    }

    func getNewsListFor(category: String, pageNum: Int, pageSize: Int, completed: @escaping ResultBlock, failed: @escaping ErrorBlock) {
        completionHandler = completed
        failHandler = failed

        if reachabilityManager.isNetworkReachable() {        //If network is available
            makeRequest(params: [RequestKeys.category: category, RequestKeys.page: String(pageNum), RequestKeys.pageSize: String(pageSize)])
        } else {
            if dbManager.isCacheAvailable() {       //If data available in db.
                dbManager.getArticles(offset: 0, limit: 0, completed: { [weak self] (articles) in
                    self?.completionHandler(articles)
                }, failed: { [weak self] (error) in
                    self?.failHandler(error)
                })
            } else {                //No data
                noInternetAlert()
                failHandler(nil)
            }
        }
    }

    func clearNewsList() {
        dbManager.deleteArticles()
    }

    func saveNewsArticles(articles: [NewsModel]) {
        dbManager.saveArticles(articles: articles)
    }
}
