//
//  FirebaseManager.swift
//  NewsApp
//
//  Created by Deepak Kumar on 16/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import Firebase

class FirebaseManager {
    let firestore = Firestore.firestore()
    private let tmpUId = "YW1hKKYxj57SR4Xlm5xa"

    lazy var userCollection = {
        firestore.collection("users")
    }()

    func addUserToFirebase() {
        // Add a new document with a generated ID
        var ref: DocumentReference?

        ref = userCollection.addDocument(data: [
            "first": "Ada",
            "last": "Lovelace",
            "born": 1815
        ]) { err in
            if let err = err {
                Logging.error("Error adding document: %@", err.localizedDescription)
            } else {
                Logging.info("Document added with ID: %@", ref!.documentID)
            }
        }
    }

    func addOtherUser() {
        // Add a second document with a generated ID.
        var ref: DocumentReference?
        ref = userCollection.addDocument(data: [
            "first": "Alan",
            "middle": "Mathison",
            "last": "Turing",
            "born": 1912
        ]) { err in
            if let err = err {
                Logging.error("Error adding document: %@", err.localizedDescription)
            } else {
                Logging.info("Document added with ID: %@", ref!.documentID)
            }
        }
    }

    func getDataFromFirebase() {
        userCollection.getDocuments { (querySnapshot, err) in
            if let err = err {
                Logging.error("Error getting documents: %@", err.localizedDescription)
            } else {
                for _ in querySnapshot!.documents {
                    //print("\(document.documentID) => \(document.data())")
                }
            }
        }
    }

    func setDocument() {
        // Will replace all content of document
        userCollection.document(tmpUId).setData([
            "first": "Asdd",
            "last": "Smithsd",
            "year": 1974
        ]) { err in
            if let err = err {
                Logging.error("Error writing document: %@", err.localizedDescription)
            } else {
                Logging.info("Document successfully written!")
            }
        }
    }

    func updateDocument() {
        //Will update the mentioned key-value only.
        userCollection.document(tmpUId).updateData(["category": "sport"]) { (_) in

        }
    }

    func setDocumentWithCodable() {
        let user = User(first: "Adam", last: "Jones", born: 1995)

        userCollection.document(tmpUId).setData(user.dictionary!) { (err) in
            if let err = err {
                Logging.error("Error writing document: %@", err.localizedDescription)
            } else {
                Logging.info("Document successfully written!")
            }
        }
    }

    func deleteDocument() {
        //To delete the document.
        userCollection.document(tmpUId).delete { err in
            if let err = err {
                Logging.error("Error removing document: %@", err.localizedDescription)
            } else {
                Logging.info("Document successfully removed!")
            }
        }
    }
}
