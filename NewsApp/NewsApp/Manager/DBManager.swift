//
//  DBManager.swift
//  NewsApp
//
//  Created by Deepak Kumar on 08/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import CoreData

typealias ResponseBlock = (_ response: [Any]?) -> Void
typealias FailureBlock = (_ error: String?) -> Void

protocol DBManagerProtocol {
    func saveArticles(articles: [NewsModel])
    func getArticles(offset: Int, limit: Int, completed:@escaping ResponseBlock, failed: @escaping FailureBlock)
    func deleteArticles()
    func isCacheAvailable() -> Bool
    func getAllArticles() -> [Article]
}

class DBManager: DBManagerProtocol {
    static let sharedInstance = DBManager()
    var managedContext: NSManagedObjectContext?
    private weak var appDelegate = UIApplication.shared.delegate as? AppDelegate

    private init() {
        guard let appDelegate = appDelegate else {
                return
        }

        self.managedContext = appDelegate.persistentContainer.viewContext
    }

    func saveArticles(articles: [NewsModel]) {
        guard let managedContext = managedContext  else {
            return
        }

        let sourceEntity = NSEntityDescription.entity(forEntityName: CoreDataEntities.newsSource, in: managedContext)!
        let articleEntity = NSEntityDescription.entity(forEntityName: CoreDataEntities.article, in: managedContext)!

        for newsArticle in articles {
            let news = NSManagedObject(entity: articleEntity, insertInto: managedContext)
            let source = NSManagedObject(entity: sourceEntity, insertInto: managedContext)

            source.setValue(newsArticle.source?.id, forKey: "id")
            source.setValue(newsArticle.source?.name, forKey: "name")
            news.setValue(source, forKey: "source")
            news.setValue(newsArticle.author, forKey: "author")
            news.setValue(newsArticle.title, forKey: "title")
            news.setValue(newsArticle.description, forKey: "desc")
            news.setValue(newsArticle.url, forKey: "url")
            news.setValue(newsArticle.urlToImage, forKey: "urlToImage")
            news.setValue(newsArticle.publishedAt, forKey: "publishedAt")
            news.setValue(newsArticle.content, forKey: "content")
        }

        appDelegate?.saveContext()
    }

    func getArticles(offset: Int, limit: Int, completed: @escaping ResponseBlock, failed: @escaping ErrorBlock) {
        var articles: [NewsModel] = []

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.article)
        if limit > 0 {
            fetchRequest.fetchOffset = offset
            fetchRequest.fetchLimit = limit
        }

        do {
            if let newsArticles = try managedContext?.fetch(fetchRequest) as? [Article] {
                articles = newsArticles.map { (news) -> NewsModel in
                    let source = NewsSource(id: news.source?.id, name: news.source?.name)
                    return NewsModel(source: source, author: news.author, title: news.title,
                                     description: news.desc, url: news.url, urlToImage: news.urlToImage,
                                     publishedAt: news.publishedAt, content: news.content)
                }

                completed(articles)
            }
        } catch let error as NSError {
            failed(error.description)
        }
    }

    func deleteArticles() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.article)

        do {
            let list = try managedContext!.fetch(fetchRequest)
            for news in list {
                managedContext?.delete(news)
            }
        } catch _ as NSError {

        }

        do {
            try managedContext!.save()
        } catch _ as NSError {

        }
    }

    func isCacheAvailable() -> Bool {
        !getAllArticles().isEmpty
    }

    func getAllArticles() -> [Article] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: CoreDataEntities.article)
        var list = [Article]()
        do {
            if let newlist = try managedContext!.fetch(fetchRequest) as? [Article] {
                list = newlist
            }
        } catch _ as NSError {
        }

        return list
    }

}
