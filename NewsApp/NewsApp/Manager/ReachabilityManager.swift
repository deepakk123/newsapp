//
//  ReachabilityManager.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import Reachability

protocol ReachabilityProtocol {
    func isNetworkReachable() -> Bool
}

class ReachabilityManager: ReachabilityProtocol {
    static var sharedInstance = ReachabilityManager()

    func isNetworkReachable() -> Bool {
        if let reachable = try? Reachability().connection == Reachability.Connection.unavailable {
            return !reachable
        }
        return false
    }
}
