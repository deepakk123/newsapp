//
//  ApiManager.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import Alamofire

protocol APIManagerProtocol {
    func requestHTTPGET(url: String, parameters: [String: Any]?, finished:@escaping ResultBlock, failed:@escaping ErrorBlock)
    func requestHTTPPost(url: String, parameters: [String: Any]?, finished:@escaping ResultBlock, failed:@escaping ErrorBlock)
}

class ApiManager: APIManagerProtocol {
    /**
     Make a HTTP GET request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPGET(url: String, parameters: [String: Any]? = nil, finished:@escaping ResultBlock, failed:@escaping ErrorBlock) {
        // HTTP Get request
        let headers: HTTPHeaders = [
                                    "Content-Type": "application/json"
                                   ]

        _ = AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default,
                       headers: headers).responseJSON { (response: AFDataResponse<Any>) in

            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    if let data = response.data {
                        finished(data)
                    }
                case .failure:
                    if let error = response.error {
                        failed(error.localizedDescription)
                    }
                }
            }
        }
    }

    func requestHTTPPost(url: String, parameters: [String: Any]? = nil, finished:@escaping ResultBlock, failed:@escaping ErrorBlock) {
        // HTTP Get request

        guard let url = URL(string: url) else { return }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters as Any)
        } catch let error {
            print("Error : \(error.localizedDescription)")
        }

        AF.request(request).responseJSON { (response) in
            DispatchQueue.main.async {
                switch response.result {
                case .success:
                    if let data = response.data {
                        finished(data)
                    }
                case .failure:
                    if let error = response.error {
                        failed(error.localizedDescription)
                    }
                }
            }
        }
    }
}
