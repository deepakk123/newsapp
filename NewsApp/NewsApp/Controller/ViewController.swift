//
//  ViewController.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let cellIdentifier = "newsCell"
    let defaultRowHeight: CGFloat = 125.0

    let categories = [Category.all, Category.business, Category.entertainment, Category.general,
                      Category.health, Category.science, Category.sports, Category.technology]

    var selectedCategoryValue = ""
    var selectedCategory: Category = .all {
        didSet {
            lblCategory.text = selectedCategory.description
            selectedCategoryValue = selectedCategory.apiRequestValue
            reloadList()
        }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblCategory: UILabel!
    lazy var selectView: SelectItemView<Category> = {
        let tmpView = SelectItemView<Category>.init(frame: tableView.frame)
        tmpView.setDefault(title: NSLocalizedString("Category", comment: ""), list: categories,
                           selectedList: [selectedCategory], multiSelection: false)
        return tmpView
    }()

    let progessView = ProgressView()
    let refreshControl = UIRefreshControl()
    lazy var activityLoader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.color = .gray
        return loader
    }()

    var newsViewModel: NewsHomeViewModelProtocol = NewsHomeViewModel()

    // MARK: - methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setEventHandler()

        reloadList()
    }

    func setupUI() {
        title = NSLocalizedString("Articles", comment: "")

        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = defaultRowHeight
        tableView.tableFooterView = activityLoader
    }

    func setEventHandler() {
        //On get result back from api.
        newsViewModel.completionHandler = { [weak self] in
            DispatchQueue.main.async {
                self?.hideLoader()
                self?.tableView.reloadData()
            }
        }

        newsViewModel.failureHandler = { [weak self] error in
            self?.hideLoader()

            if let error = error {
                self?.showAlert(message: error)
            }
        }

        //To handle to show bottom loader for next page load
        newsViewModel.loadMoreHandler = { [weak self] in
            self?.showBottomLoader()
        }

        //To show main loading view on screen.
        newsViewModel.showLoaderHandler = { [weak self] showLoader in
            DispatchQueue.main.async {
                if showLoader {
                    self?.progessView.startAnimating()
                } else {
                    self?.progessView.stopAnimating()
                }
            }
        }
    }

    func reloadList() {
        newsViewModel.getNewsArticles(for: selectedCategoryValue, refresh: false)
    }

    @objc func refreshList() {
        //fatalError()      //To test the logs in crashlytics.
        newsViewModel.getNewsArticles(for: selectedCategoryValue, refresh: true)
    }

    @IBAction func filterClicked() {
        selectView.showOver(view: view) { [weak self] (selectedItems) in
            if let category = selectedItems.first {
                self?.selectedCategory = category
            }
        }
    }

    func hideLoader() {
        //If no internet, then will hide refresh control after a delay, otherwise will show forever.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.refreshControl.endRefreshing()
        }

        hideBottomLoader()
    }

    func hideBottomLoader() {
        activityLoader.stopAnimating()
        tableView.tableFooterView = nil
    }

    func showBottomLoader() {
        tableView.tableFooterView = activityLoader
        activityLoader.startAnimating()
    }
}

// MARK: - Table View Delegate, Data Source
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        newsViewModel.articlesCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? ArticleTableViewCell else {
            return UITableViewCell()
        }

        cell.configureCell(article: newsViewModel.getNewsArticleAt(index: indexPath.row))
        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        newsViewModel.fetchNextPage(for: selectedCategoryValue, after: indexPath.row)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let newsDetailVC = UIStoryboard.main.instantiateViewController(withIdentifier: "NewsArticleDetailViewController")
            as? NewsArticleDetailViewController {
            newsDetailVC.newsDetailViewModel = newsViewModel.getNewsDetailViewModelAt(index: indexPath.row)
            navigationController?.pushViewController(newsDetailVC, animated: true)
        }
    }

}
