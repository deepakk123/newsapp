//
//  NewsArticleDetailViewController.swift
//  NewsApp
//
//  Created by Deepak Kumar on 02/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit

class NewsArticleDetailViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var newsImgView: UIImageView!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblPublishDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    var newsDetailViewModel: NewsDetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        lblTitle.text = newsDetailViewModel?.name
        lblPublishDate.text = newsDetailViewModel?.publishedAt
        lblAuthor.text = newsDetailViewModel?.author
        lblDescription.text = newsDetailViewModel?.newsDescription

        if let imgUrl = newsDetailViewModel?.imgUrl, let url = URL(string: imgUrl) {
            newsImgView.sd_setImage(with: url, completed: nil)
        }
    }

    @IBAction func shareNews(sender: UIButton) {
        // Setting description
        let message = "Checkout the News.."

        // Setting url
        guard let newsUrl = newsDetailViewModel?.newsUrl, let url = NSURL(string: newsUrl) else {
            return
        }

        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [message, url], applicationActivities: nil)

        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = sender

        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)

        // Pre-configuring activity items
        activityViewController.activityItemsConfiguration = [
        UIActivity.ActivityType.message
        ] as? UIActivityItemsConfigurationReading

        activityViewController.isModalInPresentation = true
        present(activityViewController, animated: true, completion: nil)
    }
}
