//
//  ArticleTableViewCell.swift
//  NewsApp
//
//  Created by Deepak Kumar on 02/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(article: NewsCellViewModel?) {
        if let article = article {
            lblTitle.text = article.title
            lblDescription.text = article.newsDescription

            if let imgUrl = article.imgUrl, let url = URL(string: imgUrl) {
                imgView.sd_setImage(with: url, completed: nil)
            }
        }
    }
}
