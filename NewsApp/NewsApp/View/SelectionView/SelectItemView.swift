//
//  SelectItemView.swift
//  NewsApp
//
//  Created by Deepak Kumar on 03/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit

class SelectItemView<T: CustomStringConvertible & Equatable>: UIView, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!

    typealias ItemType = T
    private var items: [ItemType] = []
    private var selectedItems: [ItemType] = []
    private var allowMultiSelection = true
    var completionHandler: (([ItemType]) -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupUI()
    }

    func setDefault(title: String = "", list: [ItemType], selectedList: [ItemType], multiSelection: Bool = true) {
        lblTitle.text = title
        items = list
        selectedItems = selectedList
        allowMultiSelection = multiSelection
    }

    func setupUI() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)

        self.backgroundColor = .clear
        tableView.tableFooterView = UIView()
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SelectItemView", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }

    @IBAction func selectAllClicked() {
        selectedItems = items
        tableView.reloadData()
    }

    @IBAction func cancelClicked() {
        hide()
    }

    @IBAction func okClicked() {
        completionHandler?(selectedItems)

        hide()
    }

    func showOver(view: UIView, completionHandler: @escaping (([ItemType]) -> Void)) {
        self.completionHandler = completionHandler
        tableView.reloadData()

        view.addSubview(self)

        self.alpha = 0
        UIView.transition(with: self,
             duration: 0.4,
              options: .curveEaseIn,
           animations: {
            self.alpha = 1
        }, completion: nil)
    }

    func hide() {
        self.alpha = 1
        UIView.transition(with: self, duration: 0.4, options: .curveEaseOut, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.removeFromSuperview()
        })
    }

    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        }

        let item = items[indexPath.row]
        cell?.textLabel?.text = item.description
        cell?.accessoryType = .none
        if selectedItems.contains(item) {
            cell?.accessoryType = .checkmark
        }

        cell?.selectionStyle = .none
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]

        if allowMultiSelection {
            if selectedItems.contains(item) {
                selectedItems.removeAll { (listItem) in
                    listItem == item
                }
            } else {
                selectedItems.append(item)
            }
        } else {
            selectedItems.removeAll()
            selectedItems.append(item)
        }

        tableView.reloadData()
    }
}
