//
//  User.swift
//  NewsApp
//
//  Created by Deepak Kumar on 16/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

struct User: Codable {
    let first: String?
    let last: String?
    let born: Int?
}
