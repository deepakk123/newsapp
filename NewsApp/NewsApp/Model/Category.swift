//
//  Category.swift
//  NewsApp
//
//  Created by Deepak Kumar on 06/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

enum Category: CustomStringConvertible {
    case all
    case business
    case entertainment
    case general
    case health
    case science
    case sports
    case technology

    var description: String {
        switch self {
        case .all:
            return "All"
        case .business:
            return "Business"
        case .entertainment:
            return "Entertainment"
        case .general:
            return "General"
        case .health:
            return "Health"
        case .science:
            return "Science"
        case .sports:
            return "Sports"
        case .technology:
            return "Technology"
        }
    }

    var apiRequestValue: String {
        switch self {
        case .all:
            return ""
        case .business:
            return "business"
        case .entertainment:
            return "entertainment"
        case .general:
            return "general"
        case .health:
            return "health"
        case .science:
            return "science"
        case .sports:
            return "sports"
        case .technology:
            return "technology"
        }
    }
}
