//
//  NewsItem.swift
//  NewsApp
//
//  Created by Deepak Kumar on 11/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation

struct NewsItem: Codable {
  let title: String
  let date: Date
  let link: String

  @discardableResult
  static func makeNewsItem(_ notification: [String: AnyObject]) -> NewsItem? {
    guard let news = notification["alert"] as? String,
      let url = notification["link_url"] as? String  else {
        return nil
    }

    let newsItem = NewsItem(title: news, date: Date(), link: url)

    NotificationCenter.default.post(
        name: Notification.Name.refreshNewsFeed,
      object: self)

    return newsItem
  }
}
