//
//  AppDelegate.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications
import SafariServices
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // Use Firebase library to configure APIs
        FirebaseApp.configure()

        //Setup for push notifications.
        UNUserNotificationCenter.current().delegate = self

        registerForPushNotifications()

        // Check if launched from notification
        let notificationOption = launchOptions?[.remoteNotification]

        if let notification = notificationOption as? [String: AnyObject],
          let aps = notification["aps"] as? [String: AnyObject] {
          NewsItem.makeNewsItem(aps)
        }

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession,
                     options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "NewsApp")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate.
                //You should not use this function in a shipping application, although it may be useful during development.

                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate.
                //You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    // MARK: - Push Notifications
    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
          .requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] granted, _ in

            Logging.debug("Permission granted: %d", granted)
            guard granted else { return }

            // Create a new notification action, with the title View on the button
            let viewAction = UNNotificationAction(
                identifier: Identifiers.viewAction, title: "View",
              options: [.foreground])

            // Define the news category, which will contain the view action. It also has a distinct identifier that your payload will need to contain.
            let newsCategory = UNNotificationCategory(
                identifier: Identifiers.newsCategory, actions: [viewAction],
              intentIdentifiers: [], options: [])

            // Register the new actionable notification.
            UNUserNotificationCenter.current().setNotificationCategories([newsCategory])

            self?.getNotificationSettings()
        }
    }

    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        Logging.debug("Notification settings: %@", settings)
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      }
    }

    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        Logging.debug("Device Token: %@", token)
    }

    func application(
      _ application: UIApplication,
      didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Logging.error("Failed to register: %@", error.localizedDescription)
    }

    func application(
      _ application: UIApplication,
      didReceiveRemoteNotification userInfo: [AnyHashable: Any],
      fetchCompletionHandler completionHandler:
      @escaping (UIBackgroundFetchResult) -> Void
    ) {
      guard let aps = userInfo["aps"] as? [String: AnyObject] else {
        completionHandler(.failed)
        return
      }

        // For background task with silent push notifications.
        if aps["content-available"] as? Int == 1 {
            //Todo: Code for background download

            completionHandler(.newData)

        } else {       //For normal notifications.

          NewsItem.makeNewsItem(aps)
          completionHandler(.newData)
        }

    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    //This is the callback you get when the app is opened by a custom action. OR by custom notification UI.
  func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse,
    withCompletionHandler completionHandler: @escaping () -> Void) {

    // Get the aps dictionary received from notification.
    let userInfo = response.notification.request.content.userInfo

    if let aps = userInfo["aps"] as? [String: AnyObject],
    let category = aps["category"] as? String,
    category == "new_podcast_available" {

    guard
        let navController = window?.rootViewController as? UINavigationController,
        let _ = navController.viewControllers[0] as? ViewController
        else {
          preconditionFailure("Invalid view configuration")
      }

      //Can Call a method from the desired class to show updated view or data.

    } else if let aps = userInfo["aps"] as? [String: AnyObject],
      let newsItem = NewsItem.makeNewsItem(aps) {

      // Check for response action to perform particular task.
      if response.actionIdentifier == Identifiers.viewAction,
        let url = URL(string: newsItem.link) {
        let safari = SFSafariViewController(url: url)
        window?.rootViewController?.present(safari, animated: true,
                                            completion: nil)
      }
    }

    completionHandler()
  }
}
