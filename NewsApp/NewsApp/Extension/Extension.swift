//
//  Extension.swift
//  NewsApp
//
//  Created by Deepak Kumar on 01/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var main = UIStoryboard(name: "Main", bundle: nil)
}

extension UIApplication {
    static var topController: UIViewController? {
        let navCtr = UIApplication.shared.windows[0].rootViewController as? UINavigationController
        return navCtr?.topViewController
    }
}

extension UIViewController {
    func showAlert(title: String = "", message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okBtn)
        self.present(alert, animated:true, completion: nil)
    }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}

extension Notification.Name {
  static let appEnteringForeground = Notification.Name("com.app.foreground")
  static let refreshNewsFeed = Notification.Name("RefreshNewsFeedNotification")
}
