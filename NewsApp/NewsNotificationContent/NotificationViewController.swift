//
//  NotificationViewController.swift
//  NewsNotificationContent
//
//  Created by Deepak Kumar on 11/09/20.
//  Copyright © 2020 Deepak Kumar. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

/**
 This class will use to show custom UI for the notification received.
 */
class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var descLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }

    func didReceive(_ notification: UNNotification) {
        self.titleLabel?.text = notification.request.content.subtitle
        self.descLabel?.text = notification.request.content.body
    }

    /**If a button tap from notification UI, and we want to use deep link then will use this.
     This tells the notification extension to open the application and deliver everything as a standard push.
     */
    @IBAction func playButtonTapped(_ sender: Any) {
      extensionContext?.performNotificationDefaultAction()
    }

}
